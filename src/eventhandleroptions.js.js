function App(){
    const handleClick = () =>{
        console.log("button was clicked")
    }
   return <div>
    <button onClick={handleClick}>Add Animal</button>
    <button onClick={() => console.log('Button clicked function option #2')}>Add Animal</button>
   </div>

}
export default App
